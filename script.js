/*Теоретичне питання.
Чому для роботи з input не рекомендується використовувати клавіатуру?
Якщо питання стосується саме подій клавіатури, що можна відстежувати при використанні поля input, то таку практику не можна вважати правильною, оскільки зв'язка "подія клавіатури - input" тут не працює. Користувач може ввести дані в поле input не лише за допомогою клавіатури комп'ютера - наприклад, якщо при введені користується мобільним телефоном або за допомогою мишки (Права клавіша - Вставити). Більш універсальний спосіб відстеження подій при роботі з input - подія input. Якщо потрібно принципово відстежити "поведінку" клавіш, в такому випадку використовуються події клавіатури, але враховуючи вищезазначені зауваження. */

/* Практичне завдання
Реалізувати функцію підсвічування клавіш:

- у файлі index.html лежить розмітка для кнопок. Кожна кнопка містить назву клавіші на клавіатурі. Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір.
- при цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною.
Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
*/

// VERSION 1
// let buttons = document.querySelectorAll(".btn");

// function deleteClass() {
//   buttons.forEach((item) => item.classList.remove("active-button"));
// }

// addEventListener("keydown", function (e) {
//   function func(x) {
//     x.classList.contains("active-button")
//       ? x.classList.remove("active-button")
//       : x.classList.add("active-button");
//   }

//   switch (e.code) {
//     case "NumpadEnter":
//       deleteClass();
//       func(buttons[0]);
//       break;
//     case "KeyS":
//       deleteClass();
//       func(buttons[1]);
//       break;
//     case "KeyE":
//       deleteClass();
//       func(buttons[2]);
//       break;
//     case "KeyO":
//       deleteClass();
//       func(buttons[3]);
//       break;
//     case "KeyN":
//       deleteClass();
//       func(buttons[4]);
//       break;
//     case "KeyL":
//       deleteClass();
//       func(buttons[5]);
//       break;
//     case "KeyZ":
//       deleteClass();
//       func(buttons[6]);
//       break;
//   }
// });

// VERSION 2 - divide keys of keyboard into groups: function keys, letters, keys of numped, etc.
let buttons = document.querySelectorAll(".btn");

function deleteClass() {
  buttons.forEach((item) => item.classList.remove("active-button"));
}

addEventListener("keydown", function (e) {
  function func(x) {
    x.classList.contains("active-button")
      ? x.classList.remove("active-button")
      : x.classList.add("active-button");
  }

  for (let i of buttons) {
    // for letters
    if (e.code.includes("Key" + i.textContent)) {
      deleteClass();
      func(i);
    }
    // for function keys
    if (e.code.includes("NumpadEnter")) {
      deleteClass();
      func(buttons[0]);
    }
  }
});
